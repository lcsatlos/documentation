# Common

* `BcConfigService.authenticationMode`
* `BcConfigService.isGenesysHybridModeEnabled`

# Bold
## User permissions:
Managed by `UserPermissionService`.
There are two levels:

* account permissions (values in enum `AccountPermissionType`)
* action permissions (guess, these are the user level permissions, values in enum `ActionPermissionType`)
* no change notifications are supported

### Account permissions

* Values provided by REST API: `users/me/account-permissions?list=${askedPermissions}`
* Supports `sync` calls only

Example:
```typescript
this._userPermissionService.hasAccountPermissionFor(AccountPermissionType.AssignmentHistory);
```

### Action permissions

* Values provided by REST API: `users/me/permissions?list=${askedPermissions}`
* Supports `sync` and `async` as well

Example:
```typescript
// sync
this.userPermissionService.hasPermissionFor(ActionPermissionType.PINVITE_ENABLED);

// async
this.isListRecordingsEnabled = await this.userPermissionService.hasPermissionForAsync(ActionPermissionType.RC_RECORDINGS_ALL)
  || await this.userPermissionService.hasPermissionForAsync(ActionPermissionType.RC_RECORDINGS_OWN);

// there is no built-in support for ANY permission but for ALL:
this._userPermissionService.hasPermissionForActionTypes([
  ActionPermissionType.ASSISTED_BROWSING_ENABLED,
  ActionPermissionType.ASSISTED_BROWSING_ACTION
]);
```

## Feature toggles:

Two different sources:

* `BcSettingsService`: dynamic, provided by REST API's GET `users/me/settings` endpoint
* `BcConfigService`: static, various Jenkins jobs can create different build targets. Environment specific values are defined `environment.(env-postfix.)ts`

### BcSettingsService:

* Defined in `settings.service.ts`.
* Settings are null, initially.
* lazy loaded behavior: settings will be fetched on first invocation of `getSettings()`
* no change notifications are supported
* named feature toggles are listed in const `BcFeatures` (24 items, located in `feature-enums.ts`)

Feature toggle access:

* Named helper function by passing actual settings object: `this._bcSettingsService.ChatFeatures.isEnabled(settings, BcFeatures.LC_NEW_VIDEO_CHAT));`
* lazy loaded `async`: `this.settingsService.getSettings().then((settings: Settings) => {...}`


### BcConfigService
Defined in `provider.ts`.

Feature toggle access:

* Direct: `BcConfigService.current` (e.g.: `this.bcConfig.current.features.geoAffinity`)
* Indirect: `BcConfigService.isFeatureSwitchOn(featureName: any): boolean` (e.g.: `this._config.isFeatureSwitchOn('assistedBrowsing')`)
* Named: `BcConfigService.isGenesysConversationEnabled()`

Feature toggle related values are expected to be constants during the lifecycle of application, no change notifications are available.


# Genesys
Has separated feature & permission logic (+endpoint level scope)

## User permissions (Authorization):
Has two sources:

* dedicated endpoint
* authorization info embedded in actual user

## Dedicated endpoint
Reverse engineered from actual GC Web app.

URL: https://api.inindca.com/api/v2/authorization/permissions?pageSize=99999

*See appendix for example response*

## Embedded auth info
Fetched with actual user info: `/api/v2/users/me?expand=authorization`

### Example response (partial):
```typescript
{
  "id": "7ebe2745-03c3-4c99-0000-b59872971f61",
    "name": "Test user",
    "state": "active",

    "authorization": {
    "roles": [
      {
        "id": "11abe1dd-e92e-448d-9098-51662792d2ef",
        "name": "AI Agent"
      },

      // ...etc

      {
        "id": "57583826-a097-438b-9968-abb41f8b9d22",
        "name": "User"
      }
    ],
    "permissions": [
      "acdscreenshare:chat:escalate",
      "acdscreenshare:session:view",
      "acdscreenshare:voice:escalate",
      "acdvideo:session:view",
      "admin",
      "alerting:alert:add",
      "alerting:alert:delete",
      "alerting:alert:edit",

      // ...etc

      "widgets:deployment:add",
      "widgets:deployment:delete",
      "widgets:deployment:edit",
      "widgets:deployment:view"
    ], 
    "permissionPolicies": [
      {
        "domain": "journey",
        "entityName": "customer",
        "allowConditions": false,
        "actionSet": [
          "add",
          "view",
          "edit",
          "delete"
        ]
      },
      {
        "domain": "journey",
        "entityName": "eventtype",
        "allowConditions": false,
        "actionSet": [
          "view"
        ]
      },

      // ...etc

      {
        "domain": "ui",
        "entityName": "agentDashboardStatus",
        "allowConditions": false,
        "actionSet": [
          "view"
        ]
      },
      {
        "domain": "conversation",
        "entityName": "message",
        "allowConditions": false,
        "actionSet": [
          "create",
          "view",
          "accept"
        ]
      }
    ]
  },
  "acdAutoAnswer": false,
    "selfUri": "/api/v2/users/7ebe2745-03c3-4c99-93dc-b59872971f61"
}
```


## Feature toggles:
URL:
```
https://apps.inindca.com/platform/api/v2/featureToggles
    ?feature=adminBetterErrors
    &feature=adminBetterLoading
    &feature=adminGraphsUsePolling
    &feature=analytics.reporting.deprecation.of.custom.interval.schedules
    &feature=analytics-ui-pure-2984-scheduled-callbacks-permission&feature=archGranularPermissions
    ...
```
### Example response
``` json
{
  "adminBetterLoading": false,
  "scripterNewLandingPage": false,
  "wfp": false,
  "adminGraphsUsePolling": false,
  "scheduleCoaching": true,
  "wfmUITransition": false,
  "dialer.automaticTimeZoneMapping": true,
  "gc2screenrecording": true,
  "wfpDecisionsDataDownload": false,
  "qm.release.keyword_spotting": false,
  "wfmHistoricAdherence": false,
  "edgeEmergencyGroupsAndHolidaySchedulesUI": true,
  "groups.enable-teams": true,
  "analytics.reporting.deprecation.of.custom.interval.schedules": false,
  "authorization.division.management": true,
  "archGranularPermissions": true,
  "wfmIntradayMonitoring": false,
  "adminBetterErrors": false,
  "coaching.usability.add-to-existing": true,
  "digitalFirstPURE2942": false,
  "analytics-ui-pure-2984-scheduled-callbacks-permission": true,
  "dialer.liveVoiceAbandonCalc": false,
  "edgeMetricsDashboard": true,
  "wfm": true
}
```

# Appendix
## Example Permissions response
Some elements are removed manually

``` json
{
  "entities": [    
    {
      "domain": "presence",
      "permissionMap": {
        "presenceDefinition": [
          {
            "domain": "presence",
            "entityType": "presenceDefinition",
            "action": "add",
            "label": "Create presence definitions",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "presence",
            "entityType": "presenceDefinition",
            "action": "delete",
            "label": "Delete presence definitions",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "presence",
            "entityType": "presenceDefinition",
            "action": "edit",
            "label": "Update presence definitions",
            "allowsConditions": false,
            "divisionAware": false
          }
        ],
        "userPresence": [
          {
            "domain": "presence",
            "entityType": "userPresence",
            "action": "edit",
            "label": "Edit another user's presence",
            "allowsConditions": false,
            "divisionAware": false
          }
        ]
      },
      "selfUri": "/api/v2/authorization/permissions"
    },
    {
      "domain": "conversation",
      "permissionMap": {
        "oneNumberFax": [
          {
            "domain": "conversation",
            "entityType": "oneNumberFax",
            "action": "receive",
            "label": "Enable One Number Fax",
            "allowsConditions": false,
            "divisionAware": false
          }
        ],
        "conference": [
          {
            "domain": "conversation",
            "entityType": "conference",
            "action": "add",
            "label": "Create an ad-hoc conference",
            "allowsConditions": false,
            "divisionAware": false
          }
        ],
        "callForwarding": [
          {
            "domain": "conversation",
            "entityType": "callForwarding",
            "action": "edit",
            "label": "Configure automatic call forwarding",
            "allowsConditions": false,
            "divisionAware": false
          }
        ],
        "transcription": [
          {
            "domain": "conversation",
            "entityType": "transcription",
            "action": "view",
            "label": "Subscribe to transcripts for any voice conversation",
            "allowsConditions": false,
            "divisionAware": false
          }
        ],
        "webmessaging": [
          {
            "domain": "conversation",
            "entityType": "webmessaging",
            "action": "accept",
            "label": "Accept a webmessage",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "conversation",
            "entityType": "webmessaging",
            "action": "create",
            "label": "Create a webmessage",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "conversation",
            "entityType": "webmessaging",
            "action": "view",
            "label": "View conversation webmessage",
            "allowsConditions": false,
            "divisionAware": false
          }
        ],
        "suggestion": [
          {
            "domain": "conversation",
            "entityType": "suggestion",
            "action": "view",
            "label": "Subscribe to suggestions for any agent assist conversation",
            "allowsConditions": false,
            "divisionAware": false
          }
        ],
        "recording": [
          {
            "domain": "conversation",
            "entityType": "recording",
            "action": "pause",
            "label": "Can perform secure pause",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "conversation",
            "entityType": "recording",
            "action": "pauseOthers",
            "label": "Can perform secure pause for others",
            "allowsConditions": false,
            "divisionAware": false
          }
        ],
        "externalTag": [
          {
            "domain": "conversation",
            "entityType": "externalTag",
            "action": "edit",
            "label": "Edit an external tag",
            "allowsConditions": false,
            "divisionAware": true
          }
        ],
        "cobrowse": [
          {
            "domain": "conversation",
            "entityType": "cobrowse",
            "action": "add",
            "label": "Add a co-browse session to a conversation",
            "allowsConditions": false,
            "divisionAware": false
          }
        ],
        "threadingTimeline": [
          {
            "domain": "conversation",
            "entityType": "threadingTimeline",
            "action": "edit",
            "label": "Edit conversation threading timeline",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "conversation",
            "entityType": "threadingTimeline",
            "action": "view",
            "label": "View conversation threading timeline",
            "allowsConditions": false,
            "divisionAware": false
          }
        ],
        "message": [
          {
            "domain": "conversation",
            "entityType": "message",
            "action": "accept",
            "label": "Accept a message",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "conversation",
            "entityType": "message",
            "action": "assign",
            "label": "Manually assign a message to an agent",
            "allowsConditions": false,
            "divisionAware": true
          },
          {
            "domain": "conversation",
            "entityType": "message",
            "action": "create",
            "label": "Create a message",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "conversation",
            "entityType": "message",
            "action": "pull",
            "label": "Manually assign a message to oneself",
            "allowsConditions": false,
            "divisionAware": true
          },
          {
            "domain": "conversation",
            "entityType": "message",
            "action": "receive",
            "label": "Create an inbound message",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "conversation",
            "entityType": "message",
            "action": "view",
            "label": "View conversation messages",
            "allowsConditions": false,
            "divisionAware": false
          }
        ],
        "participant": [
          {
            "domain": "conversation",
            "entityType": "participant",
            "action": "wrapup",
            "label": "Wrap up other users' participant on a conversation",
            "allowsConditions": false,
            "divisionAware": true
          }
        ],
        "call": [
          {
            "domain": "conversation",
            "entityType": "call",
            "action": "accept",
            "label": "Accept a call",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "conversation",
            "entityType": "call",
            "action": "add",
            "label": "Add call",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "conversation",
            "entityType": "call",
            "action": "assign",
            "label": "Manually assign a call to an agent",
            "allowsConditions": false,
            "divisionAware": true
          },
          {
            "domain": "conversation",
            "entityType": "call",
            "action": "coach",
            "label": "Coach a call",
            "allowsConditions": false,
            "divisionAware": true
          },
          {
            "domain": "conversation",
            "entityType": "call",
            "action": "monitor",
            "label": "Monitor a call",
            "allowsConditions": false,
            "divisionAware": true
          },
          {
            "domain": "conversation",
            "entityType": "call",
            "action": "pull",
            "label": "Manually assign a call to oneself",
            "allowsConditions": false,
            "divisionAware": true
          },
          {
            "domain": "conversation",
            "entityType": "call",
            "action": "record",
            "label": "Record a call",
            "allowsConditions": false,
            "divisionAware": false
          }
        ],
        "suggestionFeedback": [
          {
            "domain": "conversation",
            "entityType": "suggestionFeedback",
            "action": "add",
            "label": "Add conversation suggestion feedback",
            "allowsConditions": false,
            "divisionAware": false
          }
        ],
        "callback": [
          {
            "domain": "conversation",
            "entityType": "callback",
            "action": "accept",
            "label": "Accept a callback",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "conversation",
            "entityType": "callback",
            "action": "assign",
            "label": "Manually assign a callback to an agent",
            "allowsConditions": false,
            "divisionAware": true
          },
          {
            "domain": "conversation",
            "entityType": "callback",
            "action": "cancel",
            "label": "Cancel a callback",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "conversation",
            "entityType": "callback",
            "action": "create",
            "label": "Create a callback",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "conversation",
            "entityType": "callback",
            "action": "edit",
            "label": "Edit a callback",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "conversation",
            "entityType": "callback",
            "action": "pull",
            "label": "Manually assign a callback to oneself",
            "allowsConditions": false,
            "divisionAware": true
          }
        ],
        "communication": [
          {
            "domain": "conversation",
            "entityType": "communication",
            "action": "disconnect",
            "label": "Disconnect a communication",
            "allowsConditions": false,
            "divisionAware": true
          },
          {
            "domain": "conversation",
            "entityType": "communication",
            "action": "target",
            "label": "Target destinations based on destination division",
            "allowsConditions": false,
            "divisionAware": true
          },
          {
            "domain": "conversation",
            "entityType": "communication",
            "action": "transfer",
            "label": "Transfer other users' conversations",
            "allowsConditions": false,
            "divisionAware": true
          },
          {
            "domain": "conversation",
            "entityType": "communication",
            "action": "view",
            "label": "View other users' conversations",
            "allowsConditions": false,
            "divisionAware": true
          }
        ],
        "fax": [
          {
            "domain": "conversation",
            "entityType": "fax",
            "action": "send",
            "label": "Enable sending outbound faxes",
            "allowsConditions": false,
            "divisionAware": false
          }
        ],
        "email": [
          {
            "domain": "conversation",
            "entityType": "email",
            "action": "accept",
            "label": "Accept an email",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "conversation",
            "entityType": "email",
            "action": "assign",
            "label": "Manually assign an email to an agent",
            "allowsConditions": false,
            "divisionAware": true
          },
          {
            "domain": "conversation",
            "entityType": "email",
            "action": "create",
            "label": "Create an email",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "conversation",
            "entityType": "email",
            "action": "forward",
            "label": "Forward an email",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "conversation",
            "entityType": "email",
            "action": "pull",
            "label": "Manually assign an email to oneself",
            "allowsConditions": false,
            "divisionAware": true
          }
        ],
        "utterance": [
          {
            "domain": "conversation",
            "entityType": "utterance",
            "action": "view",
            "label": "Subscribe to transcripts for any agent assist conversation",
            "allowsConditions": false,
            "divisionAware": false
          }
        ],
        "webchat": [
          {
            "domain": "conversation",
            "entityType": "webchat",
            "action": "accept",
            "label": "Accept a web chat",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "conversation",
            "entityType": "webchat",
            "action": "assign",
            "label": "Manually assign a chat to an agent",
            "allowsConditions": false,
            "divisionAware": true
          },
          {
            "domain": "conversation",
            "entityType": "webchat",
            "action": "create",
            "label": "Create a web chat",
            "allowsConditions": false,
            "divisionAware": false
          },
          {
            "domain": "conversation",
            "entityType": "webchat",
            "action": "pull",
            "label": "Manually assign a chat to oneself",
            "allowsConditions": false,
            "divisionAware": true
          }
        ]
      },
      "selfUri": "/api/v2/authorization/permissions"
    }
  ],
  "pageSize": 99999,
  "pageNumber": 1,
  "total": 818,
  "firstUri": "/api/v2/authorization/permissions?pageSize=99999&pageNumber=1",
  "selfUri": "/api/v2/authorization/permissions?pageSize=99999&pageNumber=1",
  "lastUri": "/api/v2/authorization/permissions?pageSize=99999&pageNumber=1",
  "pageCount": 1
}
```
